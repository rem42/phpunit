<?php

namespace App\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Bundle\SecurityBundle\DataCollector\SecurityDataCollector;
use Symfony\Component\BrowserKit\Cookie;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;

class SecurityControllerTest extends WebTestCase
{
	public function testLoginWeb()
	{
		$client = static::createClient([], [
			'HTTP_HOST' => 'phpunit.local',
		]);
		$username = 'rem42';
		$password = 'd82s35';
		$url = self::$container->get('router')->generate('login');
		$crawler = $client->request('GET', $url);
		$form = $crawler->selectButton('login')->form();
		$form['_username'] = $username;
		$form['_password'] = $password;
		$client->enableProfiler();
		$client->submit($form);
		/** @var SecurityDataCollector $security */
		$security = $client->getProfile()->getCollector('security');
		$this->assertTrue(is_string($security->getUser()) && strlen($security->getUser()) > 0);
		$this->assertTrue($security->isAuthenticated(), 'Logged in user is not authenticated.');
	}

	public function testLoginWebNoPassword()
	{
		$client = static::createClient([], [
			'HTTP_HOST' => 'phpunit.local',
		]);
		$username = 'rem42';
		$password = '';
		$url = self::$container->get('router')->generate('login');
		$crawler = $client->request('GET', $url);
		$form = $crawler->selectButton('login')->form();
		$form['_username'] = $username;
		$form['_password'] = $password;
		$client->enableProfiler();
		$client->submit($form);
		/** @var SecurityDataCollector $security */
		$security = $client->getProfile()->getCollector('security');
		$this->assertFalse(is_string($security->getUser()) && strlen($security->getUser()) > 0);
		$this->assertFalse($security->isAuthenticated(), 'Logged in user is not authenticated.');
	}

	public function testLoginToken(){
		$client = static::createClient([], [
			'HTTP_HOST' => 'phpunit.local',
		]);

		$session = $client->getContainer()->get('session');

		$firewall = 'main';

		$token = new UsernamePasswordToken('rem42', null, $firewall, ['ROLE_USER']);
		$session->set('_security_'.$firewall, serialize($token));
		$session->save();

		$cookie = new Cookie($session->getName(), $session->getId());
		$client->getCookieJar()->set($cookie);

		$url = self::$container->get('router')->generate('index');
		$crawler = $client->request('GET', $url);

		$this->assertTrue($client->getResponse()->isSuccessful());
		$this->assertGreaterThan(0, $crawler->filter('html:contains("Admin Dashboard")')->count());
	}
}
