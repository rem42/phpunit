<?php

namespace App\Tests\Form;

use App\Entity\User;
use App\Form\UserType;
use Symfony\Component\Form\Test\TypeTestCase;

class UserTypeTest extends TypeTestCase
{
	public function testSubmitValidData()
	{
		$formData = [
			'username' => 'rem42',
			'email' => 'me@remy.ovh',
			'password' => [
				'first' => 'password',
				'second' => 'password',
			],
		];

		$objectToCompare = new User();

		$form = $this->factory->create(UserType::class, $objectToCompare);

		$object = new User();
		$object
			->setEmail($formData['email'])
			->setUsername($formData['username'])
			->setPassword($formData['password']['first'])
		;

		$form->submit($formData);

		$this->assertTrue($form->isSynchronized());

		$this->assertEquals($object, $objectToCompare);

		$view = $form->createView();
		$children = $view->children;

		foreach (array_keys($formData) as $key) {
			$this->assertArrayHasKey($key, $children);
		}
	}
}
